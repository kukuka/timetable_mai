import datetime
import sys
import time
import uuid
import threading

# TODO исправить проблему обнаружения локальных файлов

try:
    from bs4 import BeautifulSoup
except ModuleNotFoundError:
    import pip

    pip.main(['install', 'bs4'])
    from bs4 import BeautifulSoup

try:
    import requests
except ModuleNotFoundError:
    import pip

    pip.main(['install', 'requests'])
    import requests

try:
    from fake_headers import Headers
except ModuleNotFoundError:
    import pip

    pip.main(['install', 'fake_headers'])
    from fake_headers import Headers


class Parser(threading.Thread):
    def __init__(self, *args, **kwargs):
        super(Parser, self).__init__(*args, **kwargs)

    def run(self):
        while True:
            year = str(datetime.datetime.now().year)
            write_lock.acquire()
            try:
                group_name = group_names_local.pop(0)
                print('{} начата'.format(group_name), flush=True)
            except IndexError:
                print('Список пустой\n', flush=True)
                return 0
            write_lock.release()
            with open('example\\out\\' + group_name + '.ics', 'w', encoding='utf8') as calendar:
                calendar.write('BEGIN:VCALENDAR\n'
                               'PRODID:-//Google Inc//Google Calendar 70.9054//EN\n'
                               'VERSION:2.0\n'
                               'CALSCALE:GREGORIAN\n'
                               'METHOD:PUBLISH\n'
                               'X-WR-CALNAME:МАИ\n'
                               'X-WR-TIMEZONE:Europe/Moscow\n'
                               'X-WR-CALDESC:{}\n'
                               'BEGIN:VTIMEZONE\n'
                               'TZID:Europe/Moscow\n'
                               'X-LIC-LOCATION:Europe/Moscow\n'
                               'BEGIN:STANDARD\n'
                               'TZOFFSETFROM:+0300\n'
                               'TZOFFSETTO:+0300\n'
                               'TZNAME:MSK\n'
                               'DTSTART:19700101T000000\n'
                               'END:STANDARD\n'
                               'END:VTIMEZONE\n'.format(group_name))
                days = []
                for i in range(18):
                    for _ in range(3):
                        headers = Headers(headers=True).generate()
                        try:
                            data_group = requests.get(
                                'https://mai.ru/education/schedule/detail.php?group={}&week={}'.format(group_name, i),
                                headers=headers)
                            data_group.raise_for_status()
                            break
                        except requests.exceptions.RequestException as e:
                            print('        Ошибка, гр. {}:'.format(group_name), e, headers, flush=True)
                            time.sleep(1)
                            continue
                    time.sleep(0.015)
                    soup = BeautifulSoup(data_group.content, 'html.parser')
                    soup_data_days = soup.find_all("div", "sc-container")
                    for soup_day in soup_data_days:
                        day = soup_day.find('div', 'sc-day-header').contents[0].strip()
                        if day not in days:
                            days.append(day)
                        else:
                            continue
                        item_time = soup_day.find_all('div', 'sc-item-time')
                        item_type = soup_day.find_all('div', 'sc-item-type')
                        item_title = soup_day.find_all('span', 'sc-title')
                        lecturer = soup_day.find_all('span', 'sc-lecturer')
                        item_location = soup_day.find_all('div', 'sc-item-location')
                        for info in zip(item_time, item_type, item_title, lecturer, item_location):
                            lesson = []
                            id = uuid.uuid1()
                            for inf in info:
                                lesson.append(inf.get_text().strip())
                            if 'Военная подготовка' in lesson:
                                continue
                            calendar.write('BEGIN:VEVENT\n')
                            calendar.write('DTSTART;TZID=Europe/Moscow:{}{}T{}00\n'.format(
                                year, day[-2:] + day[0:2], lesson[0][:5].replace(':', '')))
                            calendar.write('DTEND;TZID=Europe/Moscow:{}{}T{}00\n'.format(
                                year, day[-2:] + day[0:2], lesson[0][-5:].replace(':', '')))
                            calendar.write('DTSTAMP:20190901T000000\n')
                            calendar.write('UID:{}@google.com\n'.format(id.hex))
                            calendar.write('DESCRIPTION:{}\n'.format(lesson[3]))
                            calendar.write('LOCATION:{}\n'.format(lesson[4]))
                            calendar.write('SEQUENCE:0\n')
                            calendar.write('STATUS:CONFIRMED\n')
                            calendar.write('SUMMARY:{}\n'.format(lesson[1] + ' ' + lesson[2]))
                            # calendar.write('COLOR:{}\n'.format(choice([''])))
                            calendar.write('TRANSP:OPAQUE\n')
                            calendar.write('END:VEVENT\n')
                calendar.write('END:VCALENDAR\n')
                time.sleep(0.015)
                print('{} записана'.format(group_name), flush=True)


def register():
    while True:
        time.sleep(10)
        write_lock.acquire()
        print('***************** Осталось {}*****************\n'.format(len(group_names_local)), flush=True)
        if not group_names_local:
            write_lock.release()
            break
        write_lock.release()
    return 0


if __name__ == '__main__':

    group_names = []

    try:
        with open('input_all.txt', 'r', encoding='utf8') as inp:
            tuple(map(lambda x: group_names.append(x.strip()), inp.readlines()))
    except FileNotFoundError:
        inp = input('Файла input.txt не найдено, введи номер группы (вида М1О-101С-20): ')
        group_names.append(inp)

    group_names_local = group_names.copy()
    group_names_recorded = []
    write_lock = threading.Lock()

    parsers = [Parser() for _ in range(20)]

    register_tread = threading.Thread(target=register)
    register_tread.start()
    for parser in parsers:
        parser.start()

    register_tread.join()
    for parser in parsers:
        parser.join()

    command = ''
    while command != 'q':
        command = input('Для выхода введи q: ')
    sys.exit()
