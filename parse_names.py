from bs4 import BeautifulSoup

with open('groups.html', 'r', encoding='utf8') as site:
    soup = BeautifulSoup(site.read(), 'html.parser')
    with open('input_all.txt', 'w', encoding='utf8') as input_file:
        for name in soup.find_all('a', 'sc-group-item'):
            input_file.write('{}\n'.format(name.get_text()))


